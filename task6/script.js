const nameEl = document.getElementById('name')
const surnameEl = document.getElementById('surname')
const emailEl = document.getElementById('email')
const phoneEl = document.getElementById('phoneNomber')
const messageEl = document.getElementById('message')
const btnSend = document.querySelector('.submit')
const checkbox = document.getElementById('btn')

const mail = /^[\w]{1}[\w-\.]*@[\w-]+\.[a-z]{2,4}$/i
const phone = /^\+[\d]{1}\([\d]{3}\)[\d]{2}-[\d]{2}-[\d]{3}$/

const empty = /^\s*$/
const userData = {
  name: '',
  surname: '',
  email: '',
  phone: '',
  message: '',
}

btnSend.setAttribute('disabled', 'disabled')
checkbox.addEventListener('click', () => {
  if (checkbox.checked) {
    btnSend.removeAttribute('disabled')
  } else {
    btnSend.setAttribute('disabled', 'disabled')
  }
})

btnSend.addEventListener('click', (event) => {
  event.preventDefault()
  const wrongFields = []
  if (empty.test(nameEl.value)) {
    wrongFields.push(name.id)
  }
  if (empty.test(surnameEl.value)) {
    wrongFields.push(surname.id)
  }
  if (!mail.test(emailEl.value)) {
    wrongFields.push(email.id)
  }
  if (!empty.test(phoneEl.value)) {
    if (!phone.test(phoneEl.value)) {
      wrongFields.push(phoneNomber.id)
    }
  }
  if (empty.test(messageEl.value)) {
    wrongFields.push(message.id)
  }
  if (!wrongFields.length) {
    userData.name = nameEl.value
    userData.surname = surnameEl.value
    userData.email = emailEl.value
    userData.phone = phoneEl.value
    userData.message = messageEl.value

    const cookie = document.cookie
    if (cookie.includes(`${nameEl.value + surnameEl.value}`)) {
      alert(
        `${
          nameEl.value + ' ' + surnameEl.value
        }, ваше обращение обрабатывается!`
      )
    } else {
      alert(`${nameEl.value + ' ' + surnameEl.value}, спасибо за обращение!`)
      document.cookie = `send=${nameEl.value + surnameEl.value}`
    }
  } else {
    alert(
      `Поля ${wrongFields.join(
        ', '
      )} поля заполнены не верно, пожалуйста исправьте.`
    )
  }
})

nameEl.value = localStorage.getItem('name')
nameEl.oninput = () => {
  localStorage.setItem('name', nameEl.value)
}

surnameEl.value = localStorage.getItem('surname')
surnameEl.oninput = () => {
  localStorage.setItem('surname', surnameEl.value)
}

emailEl.value = localStorage.getItem('email')
emailEl.oninput = () => {
  localStorage.setItem('email', emailEl.value)
}

phoneEl.value = localStorage.getItem('phoneNomber')
phoneEl.oninput = () => {
  localStorage.setItem('phoneNomber', phoneEl.value)
}

messageEl.value = localStorage.getItem('message')
messageEl.oninput = () => {
  localStorage.setItem('message', messageEl.value)
}
