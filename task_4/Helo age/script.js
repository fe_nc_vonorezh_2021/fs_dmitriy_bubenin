"use strict"

function getName() {

let  name_user = prompt("Ваше имя:")

    name_user = name_user.charAt(0).toUpperCase() + name_user.slice(1);

    return name_user;
}

function getAge () {

let age_user = prompt("Ваш возраст:");

    if (age_user === "" || age_user < 0) {

        alert("Введите корректный возраст!");

        return getAge();
    } 
    else {
        return age_user;
    }
}
const name = getName();
const age = getAge();

alert("Привет, " + name + ", тебе уже " + age + " Лет!");
