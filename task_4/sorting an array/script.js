`use strict`

function shuffle(arr){
	let j, temp;
	for(var i = arr.length - 1; i > 0; i--){
		j = Math.floor(Math.random()*(i + 1));
		temp = arr[j];
		arr[j] = arr[i];
		arr[i] = temp;
	}
	return arr;
}

function sortASC(a, b) {
  if (a > b) return 1;
  if (a == b) return 0;
  if (a < b) return -1;
}
 function sortDesc(a, b) {
  if (a > b) return -1;
  if (a == b) return 0;
  if (a < b) return 1;
} 

let arr = [];
for (let i = 0; i < 10; i++)
arr.push(i);
shuffle(arr);
alert (arr);


arr.sort(sortDesc);
alert (arr);

arr.sort(sortASC);
alert (arr);


let sum = 0;
 let summary = (arr) => arr.map(item => {if(item % 2 !== 0) sum += Math.pow(item,2)})
 summary(arr);

alert("Сумма квардатов нечетных элементов ="+sum);

