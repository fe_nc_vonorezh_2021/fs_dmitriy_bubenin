const radixSort = (array: number[]) => {
    const position = Array(10).fill(0).map(value => [])
    let multiplier = 1
    const maxValue = Math.max(...array)
    while( maxValue - multiplier >=0 ) {
        array.forEach( value => {
            const slice = Math.floor(value / multiplier)
            position[slice % 10].push(value)
        })
        let indexValue = 0
        position.forEach( values => {
            for(let i = 0; values.length > 0; i++){
                array[indexValue] = values.shift()
                indexValue++
            }
        })
        multiplier *= 10
    }
    return array
}

const createArr = (length: number, maxEl: number) => Array.from({ length }, () => Math.floor(Math.random() * maxEl))

const arr = createArr(20, 100)
console.log(arr)
radixSort(arr)
console.log(arr)