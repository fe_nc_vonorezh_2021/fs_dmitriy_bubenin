"use strict";
exports.__esModule = true;
exports.LinkedList = void 0;
var Node = /** @class */ (function () {
    function Node(data) {
        this.data = data;
        this.next = this.previous = null;
    }
    return Node;
}());
var LinkedList = /** @class */ (function () {
    function LinkedList(data) {
        if (data === void 0) { data = null; }
        if (data) {
            var headNode = new Node(data);
            this.head = headNode;
            this.tail = headNode;
            this.length = 1;
        }
        else {
            this.head = null;
            this.tail = null;
            this.length = 0;
        }
    }
    LinkedList.prototype.newNodeAssignment = function (currentNode, data) {
        var newNode = new Node(data);
        currentNode.previous.next = newNode;
        newNode.previous = currentNode.previous;
        newNode.next = currentNode;
        currentNode.previous = newNode;
        this.length++;
        return newNode.next;
    };
    LinkedList.prototype.removeNode = function (node) {
        var data = node.data;
        node.previous.next = node.next;
        node.next.previous = node.previous;
        this.length--;
        node.data = null;
        node.next = node.previous = null;
        return data;
    };
    LinkedList.prototype.isEmpty = function () {
        return this.length === 0;
    };
    LinkedList.prototype.get = function (index) {
        if (index < 0 || index > this.length)
            throw "Illegal argument";
        if (index > this.length / 2) {
            var i = (this.length - 1) - index;
            var tmp = this.tail;
            while (i > 0) {
                tmp = tmp.previous;
                i--;
            }
            return tmp;
        }
        else {
            var tmp = this.head;
            for (var i = 0; i < index; i++) {
                tmp = tmp.next;
            }
            return tmp;
        }
    };
    LinkedList.prototype.add = function (data) {
        var newNode = new Node(data);
        if (this.isEmpty()) {
            this.head = this.tail = newNode;
        }
        else {
            this.tail.next = newNode;
            newNode.previous = this.tail;
            this.tail = newNode;
        }
        this.length++;
    };
    LinkedList.prototype.addFirst = function (data) {
        if (this.isEmpty()) {
            this.head = this.tail = new Node(data);
        }
        else {
            var node = new Node(data);
            this.head.previous = node;
            node.next = this.head;
            this.head = node;
        }
        this.length++;
    };
    LinkedList.prototype.addAt = function (index, data) {
        if (index < 0 || index > this.length)
            throw "Illegal argument";
        if (index === 0)
            this.addFirst(data);
        else if (index === this.length)
            this.add(data);
        else {
            if (index < this.length / 2) {
                var currentNode = this.head;
                for (var i = 0; i < index; i++) {
                    currentNode = currentNode.next;
                }
                this.newNodeAssignment(currentNode, data);
            }
            else {
                var currentNode = this.tail;
                for (var i = this.length - 1; i > index; i--) {
                    currentNode = currentNode.previous;
                }
                this.newNodeAssignment(currentNode, data);
            }
        }
    };
    LinkedList.prototype.deleteFirst = function () {
        if (this.isEmpty())
            throw "The LinkedList is empty";
        var data = this.head.data;
        this.head = this.head.next;
        this.length--;
        if (this.isEmpty())
            this.tail = null;
        else
            this.head.previous = null;
        return data;
    };
    LinkedList.prototype.deleteLast = function () {
        if (this.isEmpty())
            throw "The LinkedList is empty";
        var data = this.head.data;
        this.tail = this.tail.previous;
        this.length--;
        if (this.isEmpty())
            this.head = null;
        else
            this.tail.next = null;
        return data;
    };
    LinkedList.prototype.deleteNode = function (node) {
        if (!node.previous)
            this.deleteFirst();
        else if (!node.next)
            this.deleteLast();
        else {
            this.removeNode(node);
        }
    };
    LinkedList.prototype.deleteAt = function (index) {
        if (index < 0 || index >= this.length)
            throw "Illegal argument";
        if (index === 0)
            return this.deleteFirst();
        else if (index === this.length - 1)
            return this.deleteLast();
        else {
            if (index < this.length / 2) {
                var currentNode = this.head;
                for (var i = 0; i < index; i++) {
                    currentNode = currentNode.next;
                }
                return this.removeNode(currentNode);
            }
            else {
                var currentNode = this.tail;
                for (var i = this.length - 1; i > index; i--) {
                    currentNode = currentNode.previous;
                }
                return this.removeNode(currentNode);
            }
        }
    };
    LinkedList.prototype.deleteWith = function (data) {
        var node = this.head;
        while (node) {
            if (node.data === data) {
                this.deleteNode(node);
                return;
            }
            node = node.next;
        }
    };
    LinkedList.prototype.editAt = function (index, data) {
        if (index < 0 || index >= this.length)
            throw "Illegal argument";
        this.get(index).data = data;
    };
    return LinkedList;
}());
exports.LinkedList = LinkedList;
var linkedList = new LinkedList();
console.log(LinkedList);
linkedList.add(1);
linkedList.add(2);
linkedList.add(3);
linkedList.add(4);
linkedList.addAt(2, 5);
console.log(linkedList);
linkedList.editAt(2, 8);
console.log(linkedList.get(2));
linkedList.deleteAt(2);
linkedList.deleteLast();
console.log(linkedList);
