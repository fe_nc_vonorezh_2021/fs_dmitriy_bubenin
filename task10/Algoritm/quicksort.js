function partition(list, from, to) {
    var _a, _b;
    var i = from;
    var pivot = list[to];
    for (var j = from; j < to; ++j) {
        if (list[j] < pivot) {
            _a = [list[i], list[j]], list[j] = _a[0], list[i] = _a[1];
            ++i;
        }
    }
    _b = [list[to], list[i]], list[i] = _b[0], list[to] = _b[1];
    return i;
}
function quickSortRange(list, from, to) {
    if (from < to) {
        var p = partition(list, from, to);
        quickSortRange(list, from, p - 1);
        quickSortRange(list, p + 1, to);
    }
}
function quickSort(list) {
    quickSortRange(list, 0, list.length - 1);
}
var createArray = function (length, maxEl) { return Array.from({ length: length }, function () { return Math.floor(Math.random() * maxEl); }); };
var array = createArray(10, 100);
console.log(array);
quickSort(array);
console.log(array);
