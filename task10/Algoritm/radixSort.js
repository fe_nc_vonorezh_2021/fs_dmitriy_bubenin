var radixSort = function (array) {
    var position = Array(10).fill(0).map(function (value) { return []; });
    var multiplier = 1;
    var maxValue = Math.max.apply(Math, array);
    var _loop_1 = function () {
        array.forEach(function (value) {
            var slice = Math.floor(value / multiplier);
            position[slice % 10].push(value);
        });
        var indexValue = 0;
        position.forEach(function (values) {
            for (var i = 0; values.length > 0; i++) {
                array[indexValue] = values.shift();
                indexValue++;
            }
        });
        multiplier *= 10;
    };
    while (maxValue - multiplier >= 0) {
        _loop_1();
    }
    return array;
};
var createArr = function (length, maxEl) { return Array.from({ length: length }, function () { return Math.floor(Math.random() * maxEl); }); };
var arr = createArr(20, 100);
console.log(arr);
radixSort(arr);
console.log(arr);
