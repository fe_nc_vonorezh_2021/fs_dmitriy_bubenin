import {filter, from} from 'rxjs';

const isPrime = (num) => {
    for (let i = 2; num > i; i++) {
        if (num % i === 0) {
            return false;
        }
    }
    return num > 1;
}

const arr = Array.from({length: 100}, (_, i) => i + 1);

from(arr)
    .pipe(
        filter(isPrime)
    )
    .subscribe(el => console.log(el));
