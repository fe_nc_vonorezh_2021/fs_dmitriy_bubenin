const {fromEvent, merge} = require("rxjs");

const btn1 = document.querySelector('.btn1');
const btn2 = document.querySelector('.btn2');
const btn3 = document.querySelector('.btn3');

const stream_one$ = fromEvent(btn1,'click');
const stream_two$ = fromEvent(btn2,'click');
const stream_tree$ = fromEvent(btn3,'click');

merge(stream_one$, stream_two$, stream_tree$).subscribe(() => {
    document.body.style.background = '#' + (Math.random().toString(16) + '000000').substring(2,8).toUpperCase();
})
