import {Observable} from "rxjs";

const stream$ = new Observable(observer => {
    for (let i=5; i >= 1; i--) {
        observer.next(i);
    }
    observer.error('ERROR');
    observer.complete();
})

stream$.subscribe({
    next: (value) => alert(value),
    error: (err) => alert(err),
    complete: () => alert('End')
})