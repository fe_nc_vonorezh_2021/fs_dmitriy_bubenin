import {Animal,AnimalProps} from "./animal";
import {Cat,CatProps} from "./cat";
import {Dog,DogProps} from "./dog";
import{fightingDog,FightingProps} from "./fighting";
import {cute,CuteProps} from "./cute";
import {fabricAnimal} from "./fabricAnimal";

const fab = new fabricAnimal ();

const anim =fab.create<Animal,AnimalProps>(Animal,{name:'Вася ', sex: 'м', age: 5,})
const anim1 =fab.create<Animal,AnimalProps>(Animal,{name:'Лия ', sex: 'ж', age: 2,})
anim.go()
anim1.toString()
console.log(anim)
console.log(anim1)

const cat1 =fab.create<Cat,CatProps>(Cat,{name:'Плотва', sex: 'ж', age: 3,temperament:'Меланхолик'})
const cat2 =fab.create<Cat,CatProps>(Cat,{name:'Жорик', sex: 'м', age: 2,temperament:'Сангвинник'})
console.log(cat1)
console.log(cat2)

const cute1 =fab.create<cute,CuteProps>(cute,{name:'Муся', sex: 'м', age: 4,temperament:'Флегматик', speed:50})
const cute2 =fab.create<cute,CuteProps>(cute,{name:'Фрида', sex: 'м', age: 3,temperament:'Холерик', speed:60})
console.log(cute1)
console.log(cute2)

const dog1 =fab.create<Dog,DogProps>(Dog,{name:'Ральф', sex: 'м', age: 2,color:"orange"})
const dog2 =fab.create<Dog,DogProps>(Dog,{name:'Бакс', sex: 'м', age: 3,color:"orange"})
dog2.toString()
console.log(dog1)
console.log(dog2)

const fight1 =fab.create<fightingDog,FightingProps>(fightingDog,{name:'Торас', sex: 'м', age: 4,color:"orange", fighting:'Doberman'})
const fight2 =fab.create<fightingDog,FightingProps>(fightingDog,{name:'Буря', sex: 'м', age: 3,color:"orange", fighting:'bulldog'})
fight1.toString()
console.log(fight1)
console.log(fight2)

