import {Cat,CatProps} from "./cat";

export interface CuteProps extends CatProps{
    speed:number;
}

export class cute extends Cat{
    _speed:number;

    constructor(Cat:CuteProps) {
        super(Cat);
        this._speed = Cat.speed;

    }
    set speed(newSpeed: number) {
        this._speed = newSpeed;
    }
    get speed(): number {
        return this._speed;
    }

    activity(){
        this.speed = 50
    }

    toString(): string {
        return  `${this.name}, пол: ${this.sex}, скорость: ${this._speed}`
    }
}
