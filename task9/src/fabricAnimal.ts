const log = (target: any, propertyKey: string, descriptor: PropertyDescriptor) => {
    const method = descriptor.value;
    descriptor.value = function(...args: any[]){
        console.log(`Object ${args[0].name} created!`);
        return method.apply(this, args);
    }
}

export class fabricAnimal {
    @log
    create<T, S>(type: { new(props: S): T; }, props: S): T {
        return new type(props);
    }
}

