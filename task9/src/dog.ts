import {Animal,AnimalProps} from "./animal";


export interface DogProps extends AnimalProps{
    color:string
}

export class Dog extends Animal{
    _color:string;

    constructor(Animal:DogProps) {
        super(Animal);
        this._color = Animal.color;

    }
    set color(newColor: string) {
        this._color = newColor;
    }
    get color(): string {
        return this._color;
    }

    basicColor(){
        this._color = 'Белый'
    }

    toString(): string {
        return  `${this.name}, пол: ${this.sex}, цвет: ${this.color}`
    }
}

