export interface AnimalProps {
    name: string;
    sex: string;
    age: number;
}

export class Animal {
    protected _name: string;
    protected _sex: string;
    protected _age: number;

    constructor({name, sex, age,}: AnimalProps) {
        this._name = name;
        this._sex = sex;
        this._age = age;
    }

    go() {
        console.log(`${this.name} гуляет `)
    }

      get name(): string {
        return this._name;
    }

    set name(newName: string) {
        this._name = newName;
    }

    get sex(): string {
        return this._sex;
    }

    set sex(newSex: string) {
        this._sex = newSex;
    }

    get age(): number {
        return this._age;
    }

    set age(newAge: number) {
        this._age = newAge;
    }

    toString() {
        return this.name;
    }
}