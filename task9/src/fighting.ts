import{Dog,DogProps} from "./dog";

export interface FightingProps extends DogProps{
    fighting:string;
}

export class fightingDog extends Dog{
    _fighting:string
    constructor(Dog:FightingProps) {
        super(Dog);
        this._fighting = Dog.fighting;

    }
    set fighting(newFighting: string) {
        this._fighting = newFighting;
    }
    get fighting(): string {
        return this._fighting;
    }

    toString(): string {
        return  `${this.name}, пол: ${this.sex}, бойцовая: ${this._fighting}`
    }

}


