import {Animal,AnimalProps} from "./animal";

export interface CatProps extends AnimalProps{
    temperament:string
}

export class Cat extends Animal{
    _temperament:string;

    constructor(Animal:CatProps) {
        super(Animal);
        this._temperament = Animal.temperament;

    }
    set temperament(newTemperament: string) {
        this._temperament = newTemperament;
    }
    get temperament(): string {
        return this._temperament;
    }

    basicTemperament(){
        this._temperament = 'Спокойный'
    }

    toString(): string {
        return  `${this.name}, пол: ${this.sex}, характер: ${this._temperament}`
    }
}
