class Animal {
    constructor(options) {
      this.name =options.name
      this.age = options.age
      this.pet = options.pet
}   
    voice(){
        console.log("A`m  animal")
    }
    }
//////////
class Cat extends Animal{
constructor(options) {
    super(options)
      this.color =options.color
      
}   

voice(){
super.voice()
    console.log("A`m a Cat!, miay")
}

    get ageinfo(){
        return this.age * 6;
    }

    set ageinfo(newAge){
        this.age = newAge
    }
}
////////////
class Dog extends Animal{

constructor(options) {
    super(options)
      this.color =options.color      
}   

voice(){
super.voice()
    console.log("A`m a dog!, woof")
}

    get ageinfo(){
        return this.age * 7;
    }

    set ageinfo(newAge){
        this.age = newAge
    }
}

//////////
class fightingDog extends Dog{
constructor(options) {
    super(options)
      this.speciec =options.speciec      
}   

voice(){
super.voice()
    console.log("A`m a fightingDog!,woof ")
}

}
///////////
class cute_cat extends Cat {
constructor(options) {
    super(options)
      this.speciec =options.speciec      
}   

voice(){
super.voice()
    console.log("A`m a cute_cat!, miay")
}

}

const animal = new Animal ({
    name : "Animal",
    age : 5,
    pet : "false"
})
console.log(animal);
const cat = new Cat ({
    name : "Friderika",
    age : 7,
    pet : "true" ,
    color: "gray"
})
console.log(cat);
const dog = new Dog ({
    name : "jack",
    age : 7,
    pet : "true" ,
    color: "orange"

})
console.log(dog);
const Doberman = new fightingDog ({
    name : "tsarevich",
    age : 6,
    pet : "true" ,
    color: "black",
    speciec:"Doberman"
})
console.log(Doberman);
const manchik = new cute_cat ({
    name : "leila",
    age : 3,
    pet : "true" ,
    color: "white",
     speciec:"munchkin"
})
console.log(manchik);

