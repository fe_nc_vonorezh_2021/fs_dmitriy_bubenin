"use strict"

let a = 3,
    b = 5,
    tmp;
alert(`Способ через tmp...`);
alert(`До замены : а=${a}, b=${b}`);
tmp = b;
b = a;
a = tmp;
alert(`После замены : а=${a}, b=${b}`);

alert(`Способ через Деструктурирующее присваивание:`);
let x = 1;
let y = 2;
alert(`До замены : x=${x}, y=${y}`);
[x, y] = [y, x];
a;
b;
alert(`После замены : x=${x}, y=${y}`);
