let elbuttonAdd = document.getElementById('btnAdd')
let elDelete = document.getElementById('btnDel')
let elField = document.getElementById('inputRow')
let elTable = document.getElementById('table_1')
rowsEvent()

elbuttonAdd.onclick = () => {
  if (elField.value === '') {
    alert('Введите номер строки которую хотите добавить.')
  } else if (elField.value < 1) {
    alert('Введите число, больше нуля.')
  } else if (checkInpValue()) {
    let newRow = elTable.insertRow(elField.value)
    for (let i = 0; i < 4; i++) {
      newRow.insertCell(-1).innerHTML = '-'
    }
    rowsEvent()
  }
}

function checkInpValue() {
  return (
    elField.value &&
    Number(elField.value) <= elTable.rows.length &&
    Number(elField.value) > 0
  )
}

elDelete.onclick = () => {
  if (elTable.rows.length == 1) {
    return
  } else if (elField.value === '') {
    alert('Введите номер строки которую хотите удалить.')
  } else if (elField.value <= 1) {
    alert('Введите число, больше нуля.')
  } else if (checkInpValue()) {
    elTable.deleteRow(elField.value)
  }
}

function rowsEvent() {
  elTable.addEventListener('click', function func(event) {
    let targerItem = event.target
    if (targerItem.tagName != 'TD') {
      return
    }
    targerItem.setAttribute('contenteditable', true)
  })
}
