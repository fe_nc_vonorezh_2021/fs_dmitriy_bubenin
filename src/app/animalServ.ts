import {Injectable} from "@angular/core";
import {Animal} from "./animal";
import {FamilyEnum} from "./family";

@Injectable()
export class AnimalsService {

  private animals: Animal[] = [
    new Animal('Фелисет', 1, 'Кошка', 'ж', 'рыжая', 1, FamilyEnum.Cats),
    new Animal('Лайка',2, 'Пёс', 'м','черно-белый', 3, FamilyEnum.Dogs, ),
    new Animal('Боб', 3, 'Кот', 'м', 'серый', 4, FamilyEnum.Cats,),
    new Animal('Глория', 4, 'Бегимот', 'ж', 'серый', 5, FamilyEnum.begimot,),
    new Animal('Алекс', 5, 'Лев', 'м', 'жёлтый', 3, FamilyEnum.Cats,),
    new Animal('Мелман',6,'Жираф','м','Желтый в черое пятно',5,FamilyEnum.giraffe),
    new Animal('Лейла', 7, 'Котёнок', 'ж', 'песочный', 4, FamilyEnum.Cats,),
    new Animal('Мартин', 8,'Зебра', 'м', 'Белый в черную полоску', 1, FamilyEnum.zebra,),
    new Animal('Джамбо', 9, 'Слон', 'м', 'серый', 4, FamilyEnum.elephant,),
    new Animal('Шкипер', 10, 'Пингвин', 'м', 'Черно-белый', 3, FamilyEnum.penguin,)
  ];

  constructor() { }

  getList(): Animal[] {
    return this.animals;
  }

  getFilteredList(): Animal[] {
    return this.animals.filter(animal => animal.family !== FamilyEnum.Cats);
  }

}
