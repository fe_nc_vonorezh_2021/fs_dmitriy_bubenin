import { Component } from '@angular/core';
import {AnimalsService} from "./animalServ";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [
    AnimalsService
  ]
})

export class AppComponent {
  title = 'app';
  isCatsShown = true;
  animals = this.service.getList();
  activeIdArr: number[] = [];

  constructor(private service: AnimalsService) {

  }

  CatsVisibility() {
    this.isCatsShown = !this.isCatsShown;
    this.animals = this.isCatsShown ? this.service.getList() : this.service.getFilteredList();
  }

  activeAnimal(id: number) {
    this.activeIdArr.push(id);
  }

  isActive(animalId: number) {
    return !!this.activeIdArr.find(item => item === animalId);
  }
}
